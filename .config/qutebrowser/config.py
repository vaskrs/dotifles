config.load_autoconfig(False)

# dark mode
config.set("colors.webpage.darkmode.enabled", True)

# bindings.commands = {"normal": {";w": "hint links spawn --detach mpv --force-window yes {hint-url}", "pt": "pin-tab"}}
config.bind('pt', 'tab-pin')
config.bind(';w','hint links spawn --detach mpv --force-window yes {hint-url}')
config.bind(';W','spawn --detach mpv --force-window yes {url}')
config.bind(';I','hint images spawn --output-messages wget -P "$HOME/Downloads/Qute/" {hint-url}')

# password management
config.bind('ee','spawn --userscript qute-bitwarden')
config.bind('eu','spawn --userscript qute-bitwarden --username-only')
config.bind('ep','spawn --userscript qute-bitwarden --password-only')
config.bind('eo','spawn --userscript qute-bitwarden --otp-only')

c.fonts.default_family = 'JerBrainsMono Nerd Font Mono'

c.colors.tabs.even.bg = 'grey'
c.colors.tabs.odd.bg = 'darkgrey'

c.content.blocking.method = 'both'
c.content.blocking.adblock.lists = [
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt",
        "https://easylist.to/easylist/fanboy-social.txt",
        "https://secure.fanboy.co.nz/fanboy-annoyance.txt",
        "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt",
        #"https://gitlab.com/curben/urlhaus-filter/-/raw/master/urlhaus-filter.txt",
        "https://pgl.yoyo.org/adservers/serverlist.php?showintro=0;hostformat=hosts",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/legacy.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2020.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2021.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badware.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/privacy.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badlists.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/annoyances.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/resource-abuse.txt",
        "https://www.i-dont-care-about-cookies.eu/abp/",
        "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/unbreak.txt"]

c.content.pdfjs = False
c.content.autoplay = False

c.editor.command = ["alacritty", "--class", "floating", "-e", "nvim", "{file}", "-c", "normal {line}G{column0}l"]

c.input.insert_mode.auto_load = True
# c.spellcheck.languages = ["en-US", "de-DE", "es-ES", "ko"]

c.tabs.background = True
c.tabs.title.format_pinned = '{index} {audio}'

c.url.open_base_url = True
c.url.start_pages = 'https://duckduckgo.com'
c.url.default_page = 'about:blank'

c.url.searchengines = {"DEFAULT": "https://duckduckgo.com/?q={}",
        "ddg": "https://duckduckgo.com/?q={}",
        "ig": "https://infogalactic.com/w/index.php?search={}",
        "yt": "https://www.youtube.com/results?search_query={}",
        "cpp": "https://duckduckgo.com/?sites=cppreference.com&q={}",
        "lg": "https://libgen.is/search.php?req={}",
        "arch": "https://wiki.archlinux.org/index.php?search={}",
        "aur": "https://aur.archlinux.org/packages?O=0&K={}",
        "wi": "https://en.wikipedia.org/w/index.php?search={}",
        "go": "https://www.google.com/search?q={}"}

c.window.title_format = '{perc}{current_title}{title_sep}uwu browser'
